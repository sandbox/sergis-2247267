### Overview
This module allows users to login to the site using a LDAP (M$ Active directory). If
the user is associated with a Drupal user, that user is logged in. If not, a new
user is created.
This module created because other alternatives (http://drupal.org/project/ldap, 
https://drupal.org/project/simple_ldap) have not worked for me or was to difficult to understand.

Tested only on MS AD. Use MS Active Directory Explorer (http://technet.microsoft.com/en-us/sysinternals/bb963907.aspx)
to examine your AD and determine config params for this module.

### Configuration
Settings can be found on Administration » Configuration » People » LDAP AD Auth

*GENERAL LDAP SETTINGS*
LDAP server name
- MS AD server name
LDAP DN base
- LDAP tree from which users are searched 

*ADVANCED LDAP SETTINGS*
LDAP username format
- Format for modifing username to login to LDAP (php sprinf format: "%s" means username entered on drupal side)
LDAP filter for username
- Filter used for searching user in LDAP (php sprinf format: "%s" means username entered on drupal side)
LDAP user email attribute   
- Name of LDAP attribute from user object which holds user's e-mail. This email will be used to create a user account
  that will contain other user information as well as user roles. If the email parameter is empty, the module
  will try to user the username as the email.
List of LDAP user atributes
- Put all LDAP attributes from user object which you plan to use in drupal
Roles
- Check roles which will be assigned to user when created within this module (on first login with LDAP credentials)
Debug
- If enabled various debug information will be shown. Do not use this option on production site. 
  Option available only if Devel module is enabled

*DRUPAL ROLE MAPPING*
LDAP membership atribute
- Name of LDAP attribute from user object which holds user's groups (usually "memberof")
Sync roles on each login
- If enabled then every time when user logins in drupal with LDAP credentials LDAP groups
  will be cheked and assigment of drupal roles will be changed according to mapping rules bellow.
Drupal role:administrator
- Value (DN of LDAP group) of LDAP user's object attribute (usually "memberof") which will be checked during login.
  If ("memberof") contains this DN then drupal role will be assigned to user.

*DRUPAL USER FIELD MAPPING*
- assign LDAP user's atrributes to Drupal users's fields. To use feature you have to define custom fields for user in drupal.




### Development
Refer to `ldap_ad.api.php` for hooks provided by this module.

### Credits
  * Created by Sergejs K (sergis) - https://sites.google.com/site/kanters/drupal/
  * Based on module Rest_auth created by Victor Kareh (vkareh) - http://www.vkareh.net
